#pragma once

#include "yreview_global.h"

#include <extensionsystem/iplugin.h>
#include <QVariantMap>
#include <QVariantList>

namespace yreview {
namespace Internal {

class EtMainWindow;
class YReviewDialog;
class YReviewerDialog;
class YReviewPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "yreview.json")

public:
    YReviewPlugin();
    ~YReviewPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

public slots:
    void slot_data(QVariantList data);
    void slot_actionName(void);
private:
    void triggerAction();
private:
    EtMainWindow* m_et;
    YReviewDialog* m_reviewDlg;
    YReviewerDialog* m_revierDlg;
    int m_xls = 0;
};

} // namespace Internal
} // namespace yreview
