#pragma once

#include <QtGlobal>

#if defined(YREVIEW_LIBRARY)
#  define YREVIEWSHARED_EXPORT Q_DECL_EXPORT
#else
#  define YREVIEWSHARED_EXPORT Q_DECL_IMPORT
#endif
