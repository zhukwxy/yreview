#include "yreviewdialog.h"
#include "ui_yreviewdialog.h"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <QString>
#include <projectexplorer/buildmanager.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/projectexplorericons.h>
#include <projectexplorer/projectpanelfactory.h>
#include <projectexplorer/runconfiguration.h>
#include <projectexplorer/session.h>
#include <projectexplorer/target.h>
#include <texteditor/texteditor.h>
#include <texteditor/textdocument.h>
#include <utils/textutils.h>
#include <utils/utilsicons.h>
#include <projectexplorer/session.h>
#include <projectexplorer/project.h>

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/messagemanager.h>
#include <cppeditor/cppeditorconstants.h>
#include <extensionsystem/pluginmanager.h>
#include <texteditor/texteditor.h>
#include <texteditor/textdocument.h>
#include <utils/textutils.h>
#include <utils/utilsicons.h>
#include <projectexplorer/session.h>
#include <projectexplorer/project.h>

#include <projectexplorer/buildmanager.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/projectexplorericons.h>
#include <projectexplorer/projectpanelfactory.h>
#include <projectexplorer/runconfiguration.h>
#include <projectexplorer/session.h>
#include <projectexplorer/target.h>
#include <texteditor/texteditor.h>
#include <texteditor/textdocument.h>
#include <utils/textutils.h>
#include <utils/utilsicons.h>

#include <QAction>
#include <QList>
#include <QMap>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>
#include <QTextCursor>
#include <QTextDocument>
#include <QtPlugin>
#include <QDebug>
#include <QTextBlock>
#include <QDir>

namespace yreview {
namespace Internal {

YReviewDialog::YReviewDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::YReviewDialog)
{
    ui->setupUi(this);
    init();
    initConnect();
}

YReviewDialog::~YReviewDialog()
{
    delete ui;
}

void YReviewDialog::initDocument(void)
{
    const ProjectExplorer::Project *project = ProjectExplorer::SessionManager::startupProject();
     QString projectFile = project->projectDirectory().toString();
    QString strFileName = Core::EditorManager::currentDocument()->filePath().toString();
    //获取文件名
    strFileName = strFileName.right(strFileName.size()-projectFile.lastIndexOf("/")-1);
    //获取文件行
    int line = Core::EditorManager::currentEditor()->currentLine();
    QString strFileLine = QString::number(line);


    //获取行所在的内容
    QByteArray arr = Core::EditorManager::currentDocument()->contents();
    QString str;
    str.append(QString::fromLocal8Bit(arr));
    QTextDocument textDocument(str);
    QTextBlock textBlock = textDocument.findBlockByLineNumber(line-1);
    QString strLineText = textBlock.text().trimmed();
    //包名
    QString strPackageName = project->displayName();
    //提出时间
    QString strTime = QDate::currentDate().toString("yyyyMMdd");

    ui->packageName->setText(strPackageName);
    ui->gitAddress->setText(strFileName+":"+strFileLine);
    ui->gitTime->setText(strTime);
    ui->lineEdit->setText(strLineText);

    //提出人
  //  QSettings* sett = Core::ICore::settings();
 //   QString strRevier = sett->value("reviewer").toString();
  //  ui->reViewer->setText(strRevier);

    //
    ui->codeType->clear();
    ui->useType->clear();
    QString strCode = Core::ICore::settings()->value("codeStandard").toString();
    QStringList strList;
    strList = strCode.split(",");
    if(!strList.isEmpty())
    {
        ui->codeType->addItems(strList);
    }

    strCode = Core::ICore::settings()->value("useStandard").toString();
    strList.clear();
    strList = strCode.split(",");
    if(!strList.isEmpty())
    {
        ui->useType->addItems(strList);
    }
}

void YReviewDialog::init()
{
   // struct passwd *pwd;
   // pwd = getpwuid(getuid());
 //   QString str = QObject::tr(pwd->pw_name);
  //  ui->reViewer->setText(str);
    this->setFixedSize(this->size());
}

void YReviewDialog::initConnect()
{
  //  connect(ui->reViewer,&QLineEdit::textEdited,this,&YReviewDialog::slot_textEdited);
   // connect(ui->reViewer,&QLineEdit::editingFinished, this, &YReviewDialog::slot_editingFinished);
}

void YReviewDialog::on_pushButton_clicked()
{
    do
    {
        const ProjectExplorer::Project *project = ProjectExplorer::SessionManager::startupProject();
        if(!project)
        {
            break;
        }
        QString reviewFile = project->projectDirectory().toString() + "/review/";
        QDir dir(reviewFile);
        if(!dir.exists())
        {
            dir.mkpath(reviewFile);
        }
        reviewFile += "review.txt";
        QFile file(reviewFile);
        if(!file.open(QIODevice::ReadWrite | QIODevice::Append))
        {
            break;
        }
        QTextStream text(&file);

        text << QString(QString("包名      : ") + ui->packageName->text()) << "\n";
        text << QString(QString("仓库地址   : ") + ui->gitAddress->text()) << "\n";
        text << QString(QString("问题描述   : ") + ui->comments->text()) <<"\n";
        text << QString(QString("类型      : ") + ui->codeType->currentText()) <<"\n";
        text << QString(QString("使用规范   : ") + ui->useType->currentText()) <<"\n";
        text << QString(QString("提出人     : ") + Core::ICore::settings()->value("reviewer").toString()) <<"\n";
        text << QString(QString("提出时间   : ") + ui->gitTime->text()) <<"\n";
        text << QString(QString("问题定位   : ") + ui->lineEdit->text()) <<"\n";

        text <<"\n";
        text.flush();
        file.close();


        QVariantList data;
        data << ui->packageName->text();
        data << ui->gitAddress->text();
        data << ui->comments->text();
        data << ui->codeType->currentText();
        data << ui->useType->currentText();
        data << (Core::ICore::settings()->value("reviewer").toString().isEmpty()?"default":Core::ICore::settings()->value("reviewer").toString());
        data << ui->gitTime->text();
        emit signal_data(data);

    }while(0);

    accept();
}

void YReviewDialog::slot_textEdited(const QString &text)
{

}

void YReviewDialog::slot_editingFinished()
{
   // QString strReview = ui->reViewer->text();

  //  QSettings* sett = Core::ICore::settings();
  //  sett->setValue("reviewer",strReview);
    //QString strRevier = sett->value("reviewer").toString();
}
}
}
