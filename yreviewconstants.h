#pragma once

namespace yreview {
namespace Constants {

const char ACTION_ID[] = "yreview.Action";
const char ACTION_IDNAME[] = "yreviewName.Action";
const char MENU_ID[] = "yreview.Menu";

} // namespace yreview
} // namespace Constants
