#include "yreviewplugin.h"
#include "yreviewconstants.h"
#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/inavigationwidgetfactory.h>
#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>
#include <extensionsystem/pluginmanager.h>
#include <QDebug>
#include <aggregation/aggregate.h>
#include <coreplugin/ifilewizardextension.h>
#include <coreplugin/editormanager/ieditor.h>
#include <coreplugin/idocument.h>
#include <coreplugin/editormanager/ieditorfactory.h>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <coreplugin/editormanager/editormanager.h>
#include "yreviewdialog.h"
#include <QByteArray>
#include <QString>
#include <QTextDocument>
#include <QTextBlock>
#include <texteditor/texteditor.h>
#include <coreplugin/editormanager/editormanager.h>
#include <coreplugin/editormanager/ieditorfactory.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/fileiconprovider.h>
#include <coreplugin/icore.h>
#include <coreplugin/navigationwidget.h>
#include <coreplugin/progressmanager/progressmanager.h>
#include <cpptools/cpphoverhandler.h>
#include <cpptools/cpptoolsconstants.h>
#include <texteditor/texteditoractionhandler.h>
#include <texteditor/texteditorconstants.h>
#include <texteditor/colorpreviewhoverhandler.h>
#include <texteditor/snippets/snippetprovider.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <utils/hostosinfo.h>
#include <utils/mimetypes/mimedatabase.h>
#include <utils/theme/theme.h>

#include <QCoreApplication>
#include <QStringList>

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/messagemanager.h>
#include <cppeditor/cppeditorconstants.h>
#include <extensionsystem/pluginmanager.h>
#include <texteditor/texteditor.h>
#include <texteditor/textdocument.h>
#include <utils/textutils.h>
#include <utils/utilsicons.h>
#include <projectexplorer/session.h>
#include <projectexplorer/project.h>

#include <projectexplorer/buildmanager.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/projectexplorericons.h>
#include <projectexplorer/projectpanelfactory.h>
#include <projectexplorer/runconfiguration.h>
#include <projectexplorer/session.h>
#include <projectexplorer/target.h>
#include <texteditor/texteditor.h>
#include <texteditor/textdocument.h>
#include <utils/textutils.h>
#include <utils/utilsicons.h>

#include <QAction>
#include <QList>
#include <QMap>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>
#include <QTextCursor>
#include <QtPlugin>

#include "et/etmainwindow.h"
#include "yreviewdialog.h"
#include "yreviewerdialog.h"
#include <QDate>



using namespace ExtensionSystem;
using namespace TextEditor;
using namespace  Core;
namespace yreview {
namespace Internal {

YReviewPlugin::YReviewPlugin()
{

    // Create your members
}

YReviewPlugin::~YReviewPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
}

bool YReviewPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    // Register objects in the plugin manager's object pool
    // Load settings
    // Add actions to menus
    // Connect to other plugins' signals
    // In the initialize function, a plugin can be sure that the plugins it
    // depends on have initialized their members.

    Q_UNUSED(arguments)
    Q_UNUSED(errorString)
    auto action = new QAction(tr("review code"), this);
    Core::Command *cmd = Core::ActionManager::registerAction(action, Constants::ACTION_ID,
                                                             Core::Context(Core::Constants::C_GLOBAL));
    cmd->setDefaultKeySequence(QKeySequence(tr("ctrl+Return")));
    connect(action, &QAction::triggered, this, &YReviewPlugin::triggerAction);


    auto actionName = new QAction(tr("初始化配置"), this);
    Core::Command *cmdName = Core::ActionManager::registerAction(actionName, Constants::ACTION_IDNAME,
                                                             Core::Context(Core::Constants::C_GLOBAL));

    connect(actionName, &QAction::triggered, this, &YReviewPlugin::slot_actionName);

    Core::ActionContainer *menu = Core::ActionManager::createMenu(Constants::MENU_ID);
    menu->menu()->setTitle(tr("review"));
    menu->addAction(cmdName);

    Core::ActionManager::command(TextEditor::Constants::UN_COMMENT_SELECTION);

    Core::ActionManager::actionContainer(Core::Constants::M_TOOLS)->addMenu(menu);

    m_et = new EtMainWindow();
    m_et->hide();
    m_et->initApp();
    m_reviewDlg = new YReviewDialog();
    connect(m_reviewDlg,&YReviewDialog::signal_data,this,&YReviewPlugin::slot_data);
    m_revierDlg = new YReviewerDialog();


    return true;
}

void YReviewPlugin::extensionsInitialized()
{


    if(Core::ICore::settings()->value("codeStandard").toString().isEmpty())
    {
       QString strCode;
       strCode.append("开发规范,提交规范,代码审查规范");
       Core::ICore::settings()->setValue("codeStandard",strCode);
    }
   if(Core::ICore::settings()->value("useStandard").toString().isEmpty())
   {
       QString strCode;
       strCode.append("C++/Qt代码审查规范,C++ 代码注释规范");
     Core::ICore::settings()->setValue("useStandard",strCode);
   }

    ActionContainer *contextMenu = ActionManager::actionContainer(CppEditor::Constants::M_CONTEXT);
       if (!contextMenu) // if QC is started without CppEditor plugin
           return;
    contextMenu->addSeparator();
    Command* command = Core::ActionManager::command(Constants::ACTION_ID);
    contextMenu->addAction(command);
    if(Core::ICore::settings()->value("reviewer").toString().trimmed().isEmpty())
    {
          emit Core::ActionManager::command(Constants::ACTION_IDNAME)->action()->triggered();
    }


    // Retrieve objects from the plugin manager's object pool
    // In the extensionsInitialized function, a plugin can be sure that all
    // plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag YReviewPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    m_et->closeApp();
    m_et->deleteLater();
    m_revierDlg->deleteLater();
    m_reviewDlg->deleteLater();
    return SynchronousShutdown;
}

void YReviewPlugin::slot_data(QVariantList data)
{
    static bool open = false;

    const ProjectExplorer::Project *project = ProjectExplorer::SessionManager::startupProject();
    if(project && !open)
    {
        QString str = (Core::ICore::settings()->value("reviewer").toString().isEmpty()?"default":Core::ICore::settings()->value("reviewer").toString());

        str+="_review_";
        str+= QDate::currentDate().toString("yyyyMMdd");
        QString reviewFile = project->projectDirectory().toString() + "/review/"+str;
        QFile file(reviewFile.append(".xls"));
        if(!file.exists())
        {
            m_et->newDoc();
            m_et->unProtect();
            m_et->saveAs(reviewFile);
        }
        else
        {
            m_et->openDoc(reviewFile);
        }
        m_et->unProtect();
        open = true;
    }
    data.push_front("0");
    for(int i = 0;i < data.size(); i++)
    {
       m_et->setValue(data.value(i).toString().trimmed(),i);
    }

    m_et->saveDoc();
}

void YReviewPlugin::slot_actionName()
{
    if(m_revierDlg)
    {
        m_revierDlg->init();
        m_revierDlg->show();
        m_revierDlg->raise();
        m_revierDlg->activateWindow();
    }
}

void YReviewPlugin::triggerAction()
{

  //   m_et->closeApp();

   if(Core::EditorManager::currentDocument() && Core::EditorManager::currentEditor())
   {
      m_reviewDlg->initDocument();
      m_reviewDlg->exec();

   }

}


} // namespace Internal
} // namespace yreview
