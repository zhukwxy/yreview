/********************************************************************************
** Form generated from reading UI file 'yreviewdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_YREVIEWDIALOG_H
#define UI_YREVIEWDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_YReviewDialog
{
public:
    QPushButton *pushButton;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *fileName;
    QLineEdit *fileLine;
    QLabel *label_2;
    QLineEdit *reViewer;
    QLabel *label_3;
    QLineEdit *idString;
    QLabel *label_4;
    QLineEdit *level;
    QLabel *label_5;
    QLineEdit *comments;
    QLabel *label_6;

    void setupUi(QDialog *YReviewDialog)
    {
        if (YReviewDialog->objectName().isEmpty())
            YReviewDialog->setObjectName(QString::fromUtf8("YReviewDialog"));
        YReviewDialog->resize(500, 350);
        pushButton = new QPushButton(YReviewDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(370, 300, 106, 36));
        groupBox = new QGroupBox(YReviewDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 401, 281));
        formLayout = new QFormLayout(groupBox);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        fileName = new QLineEdit(groupBox);
        fileName->setObjectName(QString::fromUtf8("fileName"));

        formLayout->setWidget(0, QFormLayout::FieldRole, fileName);

        fileLine = new QLineEdit(groupBox);
        fileLine->setObjectName(QString::fromUtf8("fileLine"));

        formLayout->setWidget(2, QFormLayout::FieldRole, fileLine);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        reViewer = new QLineEdit(groupBox);
        reViewer->setObjectName(QString::fromUtf8("reViewer"));

        formLayout->setWidget(4, QFormLayout::FieldRole, reViewer);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_3);

        idString = new QLineEdit(groupBox);
        idString->setObjectName(QString::fromUtf8("idString"));

        formLayout->setWidget(6, QFormLayout::FieldRole, idString);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_4);

        level = new QLineEdit(groupBox);
        level->setObjectName(QString::fromUtf8("level"));

        formLayout->setWidget(8, QFormLayout::FieldRole, level);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_5);

        comments = new QLineEdit(groupBox);
        comments->setObjectName(QString::fromUtf8("comments"));

        formLayout->setWidget(10, QFormLayout::FieldRole, comments);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(10, QFormLayout::LabelRole, label_6);


        retranslateUi(YReviewDialog);

        QMetaObject::connectSlotsByName(YReviewDialog);
    } // setupUi

    void retranslateUi(QDialog *YReviewDialog)
    {
        YReviewDialog->setWindowTitle(QApplication::translate("YReviewDialog", "review", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("YReviewDialog", "\347\241\256\345\256\232", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("YReviewDialog", "review", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("YReviewDialog", "\346\226\207\344\273\266:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("YReviewDialog", "\350\241\214:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("YReviewDialog", "\345\256\241\346\240\270\344\272\272:", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("YReviewDialog", "\346\240\207\350\257\206:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("YReviewDialog", "\347\272\247\345\210\253\357\274\232", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("YReviewDialog", "\346\263\250\351\207\212\357\274\232", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class YReviewDialog: public Ui_YReviewDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_YREVIEWDIALOG_H
