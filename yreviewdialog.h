#ifndef YREVIEWDIALOG_H
#define YREVIEWDIALOG_H

#include <QDialog>
#include <QVariantMap>
#include <QVariantList>

#include <optional/optional.hpp>



namespace Ui {
class YReviewDialog;
}

namespace yreview {
namespace Internal {

class YReviewDialog : public QDialog
{
    Q_OBJECT

public:
    enum id_t
    {
        PACKAGENAME_ID = 0,
        GITADDRESS_ID,
        idString,
    };
    explicit YReviewDialog(QWidget *parent = nullptr);
    ~YReviewDialog();
    void initDocument(void);
    void init(void);
    void initConnect(void);
private slots:
    void on_pushButton_clicked();
    void slot_textEdited(const QString &text);
    void slot_editingFinished();
signals:
    void signal_data(QVariantList data);
private:
    Ui::YReviewDialog *ui;
    QStringList m_lstStr;
};
}
}
#endif // YREVIEWDIALOG_H
