DEFINES += YREVIEW_LIBRARY

# yreview files
QT += core network gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
        yreviewplugin.cpp \
        yreviewdialog.cpp \
        et/etmainwindow.cpp \
    yreviewerdialog.cpp


HEADERS += \
        yreviewplugin.h \
        yreview_global.h \
        yreviewconstants.h \
        yreviewdialog.h \
        et/etmainwindow.h \
    yreviewerdialog.h


#增加wps控件的支持
#==============================================================================
INCLUDEPATH += \
                ./include/common \
                ./include/wps \
                ./include/wpp \
                ./include/et


QMAKE_CXXFLAGS += -std=c++0x -Wno-attributes

exists(/opt/kingsoft/wps-office/office6/libstdc++.so.6){
        system(ln -s /opt/kingsoft/wps-office/office6/libstdc++.so.6  libstdc++.so.6)
        LIBS += libstdc++.so.6
}

QMAKE_LFLAGS += -Wl,--rpath=\'\$\$ORIGIN\':$$[QT_INSTALL_LIBS]:/opt/kingsoft/wps-office/office6
QMAKE_LIBDIR +=  ./ $$[QT_INSTALL_LIBS]  /opt/kingsoft/wps-office/office6

greaterThan(QT_MAJOR_VERSION, 4){
        LIBS += -lrpcwpsapi_sysqt5 -lrpcetapi_sysqt5 -lrpcwppapi_sysqt5
        exists(/opt/kingsoft/wps-office/office6/libc++abi.so.1){
             QMAKE_LFLAGS += -Wl,-soname=libc++abi.so.1
        }
}
else{
        LIBS += -lrpcwpsapi -lrpcetapi -lrpcwppapi
}
#=================================================================================

# Qt Creator linking

## Either set the IDE_SOURCE_TREE when running qmake,
## or set the QTC_SOURCE environment variable, to override the default setting
isEmpty(IDE_SOURCE_TREE): IDE_SOURCE_TREE = $$(QTC_SOURCE)
#isEmpty(IDE_SOURCE_TREE): IDE_SOURCE_TREE = "/home/lx777/uos/opensrc/qt-creator-opensource-src-4.8.2"
#isEmpty(IDE_SOURCE_TREE): IDE_SOURCE_TREE = "/opt/Qt5.14.2/Tools/qt-creator-src"
isEmpty(IDE_SOURCE_TREE): IDE_SOURCE_TREE = "/opt/Qt5.11.3/Tools/qt-creator-src-4.8.2"

## Either set the IDE_BUILD_TREE when running qmake,
## or set the QTC_BUILD environment variable, to override the default setting
isEmpty(IDE_BUILD_TREE): IDE_BUILD_TREE = $$(QTC_BUILD)
#isEmpty(IDE_BUILD_TREE): IDE_BUILD_TREE = "/usr/lib/x86_64-linux-gnu/qtcreator"
#isEmpty(IDE_BUILD_TREE): IDE_BUILD_TREE = "/opt/Qt5.14.2/Tools/QtCreator"
isEmpty(IDE_BUILD_TREE): IDE_BUILD_TREE = "/opt/Qt5.11.3/Tools/QtCreator"

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on OS X
USE_USER_DESTDIR = yes

###### If the plugin can be depended upon by other plugins, this code needs to be outsourced to
###### <dirname>_dependencies.pri, where <dirname> is the name of the directory containing the
###### plugin's sources.

QTC_PLUGIN_NAME = yreview
QTC_LIB_DEPENDS += \
    # nothing here at this time

QTC_PLUGIN_DEPENDS += \
    coreplugin debugger projectexplorer

QTC_PLUGIN_RECOMMENDS += \
    # optional plugin dependencies. nothing here at this time

###### End _dependencies.pri contents ######

include($$IDE_SOURCE_TREE/src/qtcreatorplugin.pri)

FORMS += \
    yreviewdialog.ui \
    yreviewerdialog.ui
