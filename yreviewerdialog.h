#ifndef YREVIEWERDIALOG_H
#define YREVIEWERDIALOG_H

#include <QDialog>

namespace Ui {
class YReviewerDialog;
}
namespace yreview {
namespace Internal {

class YReviewerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit YReviewerDialog(QWidget *parent = nullptr);
    ~YReviewerDialog();
public:
    void setText(QString text);
    void initControl(void);
    void init(void);
private slots:
    void on_pushButton_clicked();

private:
    Ui::YReviewerDialog *ui;
};
}
}
#endif // YREVIEWERDIALOG_H
