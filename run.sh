#!/bin/bash

CMD_PATH=`dirname $0`
userdir=`env | grep ^HOME= | cut -c 6-`

qt5Creator=/usr/local/Qt5.14.1/Tools/QtCreator/lib/Qt/plugins/platforminputcontexts
qt5gcc_64=/usr/local/Qt5.14.1/5.14.1/gcc_64/plugins/platforminputcontexts
export Qt5_DIR=/usr/local/Qt5.14.1/5.14.1/gcc_64/lib/cmake/Qt5

sudo apt install git
git clone http://github.com/fcitx/fcitx-qt5.git

rm -r /home/todd/ftx-qt/fcitx-qt5/build
mkdir /home/todd/ftx-qt/fcitx-qt5/build
cd    /home/todd/ftx-qt/fcitx-qt5/build

sudo apt install gcc g++ cmake extra-cmake-modules
sudo apt install libgl1-mesa-dev libglu1-mesa-dev libxkbcommon-dev fcitx-libs-dev

cmake ../
make

libfcix=/home/todd/ftx-qt/fcitx-qt5/build/platforminputcontext/libfcitxplatforminputcontextplugin.so

sudo cp $libfcix ${qt5Creator}
sudo cp $libfcix ${qt5gcc_64}
sudo chmod +x  ${qt5Creator}/libfcitxplatforminputcontextplugin.so
sudo chmod 777 ${qt5Creator}/libfcitxplatforminputcontextplugin.so
sudo chmod +x  ${qt5gcc_64}/libfcitxplatforminputcontextplugin.so
sudo chmod 777 ${qt5gcc_64}/libfcitxplatforminputcontextplugin.so



