#include "yreviewerdialog.h"
#include "ui_yreviewerdialog.h"

#include <QAction>
#include <QSettings>
#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/messagemanager.h>
#include <cppeditor/cppeditorconstants.h>
#include <QInputDialog>
#include <QLineEdit>
#include <QDir>
namespace yreview {
namespace Internal {

YReviewerDialog::YReviewerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::YReviewerDialog)
{
    ui->setupUi(this);
    //ui->comboBox->addAction()
    initControl();
    //ui->comboBox->addAction(new )
}

YReviewerDialog::~YReviewerDialog()
{
    delete ui;
}

void YReviewerDialog::setText(QString text)
{
    ui->lineEdit->setText(text);
}

void YReviewerDialog::initControl()
{
    //代码规范控件
    QAction* act = new QAction("添加",ui->comboBox);
    connect(act,&QAction::triggered,[this](){
            bool ok = false;
            QString text = QInputDialog::getText(this,"获取规范类型",tr("输入文本"), QLineEdit::Normal, "",&ok);
            if(ok && !text.isEmpty())
            {
               ui->comboBox->addItem(text);
            }
    });
    ui->comboBox->addAction(act);

    act = new QAction("删除",ui->comboBox);
    connect(act,&QAction::triggered,[this](){
        ui->comboBox->removeItem(ui->comboBox->currentIndex());
    });
    ui->comboBox->addAction(act);
    ui->comboBox->setContextMenuPolicy(Qt::ActionsContextMenu);

//使用规范控件
    act = new QAction("添加",ui->comboBox_2);
    connect(act,&QAction::triggered,[this](){
            bool ok = false;
            QString text = QInputDialog::getText(this,"获取规范类型",tr("输入文本"), QLineEdit::Normal, "",&ok);
            if(ok && !text.isEmpty())
            {
               ui->comboBox_2->addItem(text);
            }
    });
    ui->comboBox_2->addAction(act);

    act = new QAction("删除",ui->comboBox_2);
    connect(act,&QAction::triggered,[this](){
        ui->comboBox_2->removeItem(ui->comboBox_2->currentIndex());
    });
    ui->comboBox_2->addAction(act);
    ui->comboBox_2->setContextMenuPolicy(Qt::ActionsContextMenu);

}

void YReviewerDialog::init()
{
    QSettings* sett = Core::ICore::settings();
    QString strRevier = sett->value("reviewer").toString();
    ui->lineEdit->setText(strRevier);
    ui->comboBox->clear();
    ui->comboBox_2->clear();
    QString strCode = Core::ICore::settings()->value("codeStandard").toString();
    QStringList strList;
    strList = strCode.split(",");
    if(!strList.isEmpty())
    {
        ui->comboBox->addItems(strList);
    }

    strCode = Core::ICore::settings()->value("useStandard").toString();
    strList.clear();
    strList = strCode.split(",");
    if(!strList.isEmpty())
    {
        ui->comboBox_2->addItems(strList);
    }
}

void YReviewerDialog::on_pushButton_clicked()
{
    //保存初始化信息
    accept();
    QString str = ui->lineEdit->text().trimmed();
    QSettings* sett = Core::ICore::settings();
    sett->setValue("reviewer",str);
    //QStringList lstStr;
     QString strCode;
    for(int i = 0; i < ui->comboBox->count(); ++i)
    {
        if(ui->comboBox->itemText(i).trimmed().isEmpty())
        {
            continue;
        }
        strCode.append(ui->comboBox->itemText(i));
        if(i != ui->comboBox->count()-1)
        {
            strCode.append(",");
        }
    }
   // if(!strCode.isEmpty())
    {
      Core::ICore::settings()->setValue("codeStandard",strCode);
    }
    strCode.clear();

    for(int i = 0; i < ui->comboBox_2->count(); ++i)
    {
        if(ui->comboBox_2->itemText(i).trimmed().isEmpty())
        {
            continue;
        }
        strCode.append(ui->comboBox_2->itemText(i));
        if(i != ui->comboBox_2->count()-1)
        {
            strCode.append(",");
        }
    }
  //  if(!strCode.isEmpty())
    {
      Core::ICore::settings()->setValue("useStandard",strCode);
    }
}
}
}
